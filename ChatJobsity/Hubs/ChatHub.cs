﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatJobsity.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string message) => await Clients.All.SendAsync("ReceiveMessage", message);
    }
}
